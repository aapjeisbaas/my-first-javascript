// function to append text to the document
function createParagraph() {
  let para = document.createElement('p');
  para.textContent = 'You clicked the button!';
  document.body.appendChild(para);
}

// get a list of all the buttons
const buttons = document.querySelectorAll('button');

// Setup an event listener for all the buttons in the list to execute function on click event
for(let i = 0; i < buttons.length ; i++) {
  buttons[i].addEventListener('click', createParagraph);
}